IRC
===

IRC (Internet Relay Chat) is the bread and butter of darkscience.

It is both a social hub and a service offered free of charge to other communities.

To find out technical details follow the [technical reference](#about),
or [read the beginners guide](../irc/beginners-guide.md).

All communities must follow our [network rules](../irc/rules-network.md).

Lobby/Social [group rules](../irc/rules-lobby.md) 
are only enforced in the `#darkscience` channel.

PRIVACY
=======

Privacy first is a hard thing for an IRC service to promise, IRC itself is
 inherently insecure, and there is nothing preventing operators from backdooring
 the IRCd's themselves to snoop on otherwise private communications.

Truthfully we can never get to a state where we can be fully trusted. We expose
 our configuration files and we assume you understand that we do our best to
 keep your information private.

That said, there are other considerations when it comes to privacy, such as VPN,
 tor and other darknet/hidden-service networks operating over the internet.

We pride ourselves on hosting an IRC hidden service on tor, with i2p and cjdns
 to follow soon.

Our tor address is: `darksci3bfoka7tw.onion`

irc://darksci3bfoka7tw.onion/darkscience should work if you have the ability to
 open irc:// protocol handles.

ABOUT
=====

The network itself is constructed of 7 nodes as of writing.

6 leafs and a core.

4 of those leaf nodes are geographically balanced, this is the normal case.
 when connecting to `irc.darkscience.net` you should be connecting to the
 closest available irc node.

Currently these are:

 - EMEA/Europe:  `lavender`
 - NCSA/US-East: `celadon`
 - NCSA/US-West: `indigo`
 - APAC/Asia:    `cinnabar`

There are many exceptions to this flow, for instance connections to our tor
 hidden address will go to a special node named `verus`.

Alternatively you may be using the domain 'darchoods.net'. Which will put you on
 a machined named `pallet`. None of the leafs share networking or hardware
 and as such can be pointed to individually in an emergency.

We prefer you to use the 'happy path', connections are usually more stable and
 communication with someone in your region has the lowest latency.
 Unless of course you are using hidden services.
