DarkScience Services: OAUTH
===========================

Since DarkScience is primarily an IRC Backed community, it makes sense to have
 it as the core of any service we offer.

We now provide an `OAUTH` service that will allow you to authenticate with
 other services on the DarkScience network using a single set of credentials
 that are derived from your IRC service account.

We also offer application tokens and such for 3rd party applications, for now,
 these need to be requested from a member of staff.
