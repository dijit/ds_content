Bouncer
=======

Many people find it hard to stay connected 24/7, [bouncers] typically solve this
 problem, however they can be expensive to run. Since we're already hosting IRC
 why not use us to host your bouncer too?

Like all DS services, this is free of charge. We use [ZNC] (since we have a
 contributor on staff) with specific extensions for the iOS IRC App palaver.
 (for push notifications)

Since users connecting from our bouncer to other networks will carry a
 darkscience hostmask we vet all users before granting access.

Please contact a member of [staff] for more information.

[ZNC]: http://znc.in
[bouncers]: #TODO: wikipedia/irc/bouncers
[staff]: ../irc/staff.md
