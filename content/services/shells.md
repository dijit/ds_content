DarkScience Shells
==================

Darkscience offers a free to use shell service for trusted members.

The domain is: **sh.drk.sc**

MAIL
----

Along with shell access you will also get an email address on the server itself

*username*@sh.drk.sc will go to a local Maildir in your home folder.

the `mail` command is configured by default to look here, there is also [mutt]

You can also access mail via IMAP(S) using the same credentials as the shell.
 SMTP is also supported the same way, but discouraged.

WEB
---

Users have the ability to serve static html/js pages (over https) via
 `~/public_html` which will appear in `https://sh.drk.sc/~username/`

For example dijit's is: [https://sh.drk.sc/~dijit/]

LIMITS
------

This machine is a proof-of-concept, the intention was to replace it with a
 a better multi-user operating system such as FreeBSD. For now the limited disk
 capacity should be considered by all those using the service as it is shared.

[mutt]: #TODO: Mutt Maildir documentation here
[https://sh.drk.sc/~dijit/]: https://sh.drk.sc/~dijit/
