Pastebin (SNIPPT)
=================

DarkScience runs a private pastebin which was written in-house.

Pastebin software and services are incredibly competitive, thus we do not
advocate use of our pastebin over any other, we don't do anything particularly
or inherently different. Admins could still read your pastes, although they
really couldn't possibly care less.

API
---

Snippt has a `curl`/POST API, firing a POST to snippt will cause it to return a
URL.

the normal case:

```sh
curl -F 'paste=<-' http://s.drk.sc
```

syntax highlighting, useful for adding to vim:

```sh
curl -F 'paste=<-' http://s.drk.sc | sed 's/$/?guess/g'
```


Open Source
-----------

Like all projects we develop ourselves, snippt is open source software visible
on our [gitlab instance], the repository is under [darkscience/snippt]

[gitlab instance]: https://git.drk.sc
[darkscience/snippt]: https://git.drk.sc/darkscience/snippt
