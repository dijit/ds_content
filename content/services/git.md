GIT
===

https://git.drk.sc

Git is provided free of charge to darkscience members, we have not had abuse yet
 but if we do we reserve the right to terminate services. (We will give warning
 and we will not delete anything)

The software we're using is [gitlab], we enabled all the bells and whistles
 excluding mattermost, which we will be trying to integrate with IRC in the near
 future.

Right now accounts are independent of IRC, work is being undertaken now to
 attempt integration with our IRC service backend, allowing single-sign-on.

There is also a shared CI runner which is also free to use for all DS patrons.

To learn how to use CI in your project please refer to the official
 [documentation]

Users are limited to 100 public or private repositories, please contact a member
of staff if there are issues.

[documentation]: #TODO: add gitlab-ci.yml doc

