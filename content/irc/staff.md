Staff
=====

All staff will be identified on IRC with a 'staff.darkscience.net' hostmask.
This is a protected mask and will only be used by staff members, even if their
names do not match with the supplied handles on this page.

Staff members are required to idle in `#support`, mass highlighting people in
that channel is not only allowed, it is encouraged.

**Global Ops:**

dijit
-----

Discipline: Infrastructure Engineer.

Timezone: CEST (UTC+1)

derecho
-------

Discipline: Embedded Programmer

Timezone: CEST (UTC+1)

scub/skoobie
------------

Discipline: Security/Systems Engineer

Timezone: EST (UTC-6)

liothen
-------

Discipline: DevOps

Timezone: PST (UTC-8)

kylef
-----

Discipline: Server Programmer, iOS developer

Timezone: fucker is never around.

**Server Ops:**

narada
------

xLink
-----


