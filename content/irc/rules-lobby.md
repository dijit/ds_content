Lobby Rules
===========

It is important to note that lobby rules only apply to the channel
 `#darkscience` and complement the [network rules].
 These rules do not extend to other channels, and other channels may have
 significanly different rules to these.

Rules for engagement with network lobby:

 0: Do not act in a way that makes us write another rule.

[network rules]: rules-network.md
