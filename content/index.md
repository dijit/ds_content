Meet DarkScience
----------------

The IRC network and social group for technically literate people who care about
 connecting with other privacy focused technocrats.

Some have called the effort: "a virtual hackerspace", we pride ourselves on
 running community services that many people can enjoy for free.

Connecting
----------

You should be able to click the link: ircs://irc.darkscience.net/darkscience
 or connect manually to irc.darkscience.net (port: 6697 SSL) and join
 `#darkscience`

Alternatively you can connect with [mibbit] or [kiwi] to stay in the browser.

For more information about IRC please visit the link below.

Services:
---------

- [IRC](services/irc.md) ∆
- [GIT](services/git.md)
- [FORUM](services/forum.md)
- [OAUTH](services/oauth.md)
- [SHELLS](services/shells.md)
- [BOUNCER](services/ZNC.md)
- [Pastebin](services/snippt.md)

[kiwi]: https://kiwiirc.com/client/irc.darkscience.net:+6697/darkscience
[mibbit]: http://www.mibbit.com/#darkscience@irc.darkscience.net:+6697
