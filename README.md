DS Website Content
==================

This is the official repository of darkscience content for the new site going
forward.

The work is expected to be palatable to people who have designed websites AND
static site creation systems such as pelican/sphinx before.

[index.md](content/index.md) is the expected root of the site.
